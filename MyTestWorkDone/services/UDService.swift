//
//  File9.swift
//  MyTestWorkDone
//
//  Created by Serj Potapov on 23.09.2022.
//


import Foundation

class UDService {
    
    static let shared = UDService()
    private init() {}
    let ud = UserDefaults.standard
    
    let cityKey = "city"
    
    func setCity(_ city: String) {
        ud.set(city, forKey: cityKey)
    }
    
    func getCity() -> String? {
        if let city = ud.string(forKey: cityKey) {
            return city
        } else {
            return nil
        }
    }
    
}
