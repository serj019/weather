//
//  File8.swift
//  MyTestWorkDone
//
//  Created by Serj Potapov on 23.09.2022.
//

import Foundation

class NetworkService {
    static let shared = NetworkService()
    private init() {}
    
    private let apiKey = "b9c81050cef9855eeaca27cf97ae5d26"
    
    private func createURL(cityName: String) -> URL? {
        let tunnel = "https://"
        let server = "api.openweathermap.org"
        let method = "/data/2.5/forecast"
        let params = "?q=\(cityName)&appid=\(apiKey)"
        let string = tunnel + server + method + params
        let url = URL(string: string)
        
        return url
    }
    
    func getForecast(city: String) async -> ForecastData? {
        
        guard let url = self.createURL(cityName: city) else { return nil }
        guard let response = try? await URLSession.shared.data(from: url) else { return nil }
       
        let data = response.0
        
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        guard let forecastData = try? jsonDecoder.decode(ForecastData.self, from: data) else { return nil }
        
        return forecastData
    }
    
}
