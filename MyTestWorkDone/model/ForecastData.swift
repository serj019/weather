//
//  File7.swift
//  MyTestWorkDone
//
//  Created by Serj Potapov on 23.09.2022.
//

import Foundation

struct ForecastData: Decodable {
    
    var city: City
    var list: [Forecast]
    
    struct Forecast: Decodable {
        var dt: Int
        var main: Main
        var wind: Wind
        
        struct Main: Decodable {
            var temp: Double
            var pressure: Double
        }
        
        struct Wind: Decodable {
           var speed: Double
            var deg: Double
        }
    }
    
    struct City: Decodable {
        var name: String
    }
    
}
