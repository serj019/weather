//
//  File3.swift
//  MyTestWorkDone
//
//  Created by Serj Potapov on 23.09.2022.
//

import UIKit

class SelectCityController: UIViewController {
    
    let mainView = SelectCityView()
    let cities = ["Moscow", "Kazan'", "Ulyanovsk", "Novosibirsk" , "Orel"]

    override func viewDidLoad() {
        super.viewDidLoad()
        view = mainView
        
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }


}

extension SelectCityController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CityCell.reuseID) as! CityCell
        cell.cityLabel.text = cities[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let weatherVC = WeatherController()
        let city = cities[indexPath.row]
        weatherVC.city = city
        UDService.shared.setCity(city)
        weatherVC.modalPresentationStyle = .fullScreen
        self.present(weatherVC, animated: true)
    }
    
}
