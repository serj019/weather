//
//  File4.swift
//  MyTestWorkDone
//
//  Created by Serj Potapov on 23.09.2022.
//

import UIKit

class WeatherController: UIViewController {
    
    var city: String = ""
    var weatherData: ForecastData? {
        didSet {
            mainView.tableView.reloadData()
        }
    }
    
    var oneDayWeatherData: Array<ForecastData.Forecast>.SubSequence? {
        guard let weatherData = weatherData else { return nil }
        let forecasts = weatherData.list[0...7]
        return forecasts
    }
    
    var mainView = WeatherView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addActions()
        self.view = mainView
        mainView.label.text = city
        getWeather()
        mainView.tableView.dataSource = self
        
    }
    
    func getWeather() {
        Task {
            self.weatherData = await NetworkService.shared.getForecast(city: city)
            //print(weatherData!.city.name)
            //print(weatherData!.list[0].main.temp - 273)
            
        }
    }
    
    
    func getDirection(deg: Double) -> String {
        var getDirection: String = ""
        switch deg {
        case 22.5...67.5: getDirection = "СВ"
        case 67.5...112.5: getDirection =  "В"
        case 112.5...157.5: getDirection =  "ЮВ"
        case 157.5...202.5: getDirection =  "Ю"
        case 202.5...247.5: getDirection =  "ЮЗ"
        case 247.5...292.5: getDirection =  "З"
        case 292.5...337.5: getDirection =  "СЗ"
        default: getDirection =  "С"
        }
        return getDirection
    }
    
    func getTimeString(unixTime: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(unixTime))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let result = dateFormatter.string(from: date)
        return result
    
    }
    
    func addActions()
    {
        let actionBack = UIAction { _ in
            let vc = SelectCityController()
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true)
            
        }
        mainView.backButton.addAction(actionBack, for: .touchUpInside)
    }
}



extension WeatherController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return oneDayWeatherData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ForecastCell.reuseID) as! ForecastCell
        let forecast = oneDayWeatherData![indexPath.row]
        cell.timeLabel.text = "\(getTimeString(unixTime: forecast.dt))"
        cell.tempLabel.text = "\(Int(forecast.main.temp - 273))°С"
        cell.pressureLabel.text = "\(Int(forecast.main.pressure / 1.3)) мм.рт.ст"
        cell.windSpeedLabel.text = "\(Int(forecast.wind.speed)) м/с"
        cell.windDirectionLabel.text = getDirection(deg: forecast.wind.deg)
        return cell
    }
    
}
