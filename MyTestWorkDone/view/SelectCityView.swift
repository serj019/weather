//
//  q1.swift
//  MyTestWorkDone
//
//  Created by Serj Potapov on 23.09.2022.
//

import UIKit

class SelectCityView: UIView {
    let tableView = UITableView()
    let titleLabel = UILabel()
    
    
    init() {
        super.init(frame: CGRect())
        setViews()
        setContraints()
    }
    
    func setViews() {
        
        backgroundColor = .white
        
        self.backgroundColor = .white
        titleLabel.font = UIFont(name: "AvenirNext-bold", size: 18)
        titleLabel.text = "Выберите город"
        tableView.register(CityCell.self, forCellReuseIdentifier: CityCell.reuseID)
    }
    
    func setContraints() {
        addSubview(titleLabel)
        addSubview(tableView)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 4),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
            tableView.centerXAnchor.constraint(equalTo: centerXAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)])
        
      
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
