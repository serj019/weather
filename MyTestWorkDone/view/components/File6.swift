//
//  File6.swift
//  MyTestWorkDone
//
//  Created by Serj Potapov on 23.09.2022.
//

import UIKit

class ForecastCell: UITableViewCell {
    
    static let reuseID = "ForecastCell"
    
    let timeLabel = UILabel()
    let tempLabel = UILabel()
    let windDirectionLabel = UILabel()
    let windSpeedLabel = UILabel()
    let pressureLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setConstraints()
    }
    
    
    func setConstraints() {
        let windStack = UIStackView(arrangedSubviews: [windDirectionLabel, windSpeedLabel])
        windStack.axis = .vertical
        windStack.spacing = 2
        
        let stack = UIStackView(arrangedSubviews: [timeLabel, tempLabel, windStack, pressureLabel])
        stack.axis = .horizontal
        stack.spacing = 6
        
        addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            stack.centerXAnchor.constraint(equalTo: centerXAnchor),
            stack.centerYAnchor.constraint(equalTo: centerYAnchor),
            stack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4)])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
