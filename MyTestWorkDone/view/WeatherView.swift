//
//  q2.swift
//  MyTestWorkDone
//
//  Created by Serj Potapov on 23.09.2022.
//

import UIKit

class WeatherView: UIView {

    let label = UILabel()
    let tableView = UITableView()
    let backButton = UIButton()
    
    init() {
        super.init(frame: CGRect())
        setConstraints()
        setViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setViews() {
        backButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        backButton.backgroundColor = .red
        backButton.setTitle("Back", for: .normal)
        
        backgroundColor = .white
        label.font = UIFont(name: "AvenirNext-bold", size: 18)
        tableView.register(ForecastCell.self, forCellReuseIdentifier: ForecastCell.reuseID)
    }
    
    func setConstraints() {
        
        
        addSubview(label)
        addSubview(tableView)
        addSubview(backButton)
    
        backButton.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 20),
            tableView.centerXAnchor.constraint(equalTo: centerXAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)])
        
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: centerXAnchor),
            label.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 4)
        ])
        
        NSLayoutConstraint.activate([
            backButton.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,constant: 10),
            backButton.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 10)
        ])
    }

}
